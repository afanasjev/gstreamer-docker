#!/bin/bash
ARCH=$(uname -m)

if [[ "$ARCH" = "armv7l" ]]
then
    IMAGE_TAG="mmastrac/gst-omx-rpi:latest"
    GST_DECODER="omxh264dec"
    GST_VIDEO_SINK="fbdevsink"
else
    IMAGE_TAG="afanasjev/gstreamer-docker:latest"
    GST_DECODER="avdec_h264"
    GST_VIDEO_SINK="autovideosink"
fi

echo -e "\e[44mTarget arch: $ARCH ...\e[0m"

# Start stream client
eval ./start_stream_client.sh $IMAGE_TAG $GST_DECODER $GST_VIDEO_SINK ${@}
