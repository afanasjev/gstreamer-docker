#!/bin/bash
CONTAINER_NAME="camera-client"

IMAGE_TAG=${1:-"afanasjev/gstreamer-docker:latest"}
GST_DECODER=${2:-"avdec_h264"}
GST_VIDEO_SINK=${3:-"autovideosink"}
HOST=${4:-"pi-hole.local"}
PORT=${5:-"5000"}

# Start old container
eval docker rm -f $CONTAINER_NAME

# Start stream client
eval docker run -i --privileged \
	--name $CONTAINER_NAME \
	-p $PORT:$PORT \
	-e DISPLAY=$DISPLAY \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
		$IMAGE_TAG \
			gst-launch-1.0 -v tcpclientsrc port=$PORT host=$HOST !\
				gdpdepay ! '"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96"' !\
					rtpjitterbuffer ! rtph264depay ! h264parse ! $GST_DECODER ! videoconvert ! $GST_VIDEO_SINK sync=false async=false
