#!/bin/bash

PORT=${1:-5000}
H264_STREAM="raspivid -n -w 800 -h 430 -fps 60 -vf -hf -t 0 -o -"

# Start stream server
eval $H264_STREAM | gst-launch-1.0 -v fdsrc ! h264parse ! rtph264pay config-interval=10 pt=96 ! gdppay ! tcpserversink port=$PORT host=0.0.0.0
