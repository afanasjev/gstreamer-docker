#!/bin/bash

ARCH=$(uname -m)
HOST=${1:-"pi-hole.local"}
PORT=${2:-5000}

if [[ "$ARCH" = "armv7l" ]]
then
    GST_DECODER="omxh264dec"
    GST_VIDEO_SINK="xvimagesink double-buffer=true"
else
    GST_DECODER="avdec_h264"
    GST_VIDEO_SINK="autovideosink"
fi

echo -e "\e[44mTarget arch: $ARCH ...\e[0m"
eval export DISPLAY=:0.0
eval gst-launch-1.0 -v tcpclientsrc port=$PORT host=$HOST !\
	gdpdepay ! '"application/x-rtp, media=(string)video, clock-rate=(int)90000, encoding-name=(string)H264, payload=(int)96"' !\
		rtpjitterbuffer ! rtph264depay ! h264parse ! $GST_DECODER ! videoconvert ! $GST_VIDEO_SINK sync=false async=false
