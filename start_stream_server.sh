#!/bin/bash
IMAGE_TAG="mmastrac/gst-omx-rpi:latest"
CONTAINER_NAME="camera-server"

PORT=${1:-5000}
H264_STREAM="raspivid -n -w 640 -h 480 -fps 30 -b 1000000 -vf -hf -t 0 -o -"

# Start old container
eval docker rm -f $CONTAINER_NAME

# Start stream server
echo $H264_STREAM
echo docker run -i --privileged \
		--name $CONTAINER_NAME \
		-p $PORT:$PORT \
		$IMAGE_TAG \
			gst-launch-1.0 -v fdsrc !\
				h264parse !\
				rtph264pay config-interval=10 pt=96 !\
				gdppay !\
				tcpserversink port=$PORT host=0.0.0.0 sync=false async=false
