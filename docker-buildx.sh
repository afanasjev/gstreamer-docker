#!/bin/bash

IMAGE_TAG="afanasjev/gstreamer-docker:latest"
IMAGE_TARBALL_FILE="gstreamer-docker.tar.gz"
DOCKER_FILE_NAME="Dockerfile"
CURRENT_WORKING_DIR=$(pwd)
BUILD_PLATFORM="linux/amd64"
# ===========================================================================================================================================

remove_docker_image="docker rmi -f $IMAGE_TAG"
save_docker_image="docker save $IMAGE_TAG | gzip > $CURRENT_WORKING_DIR/$IMAGE_TARBALL_FILE"
build_docker="docker buildx build --platform $BUILD_PLATFORM -f $DOCKER_FILE_NAME -t $IMAGE_TAG --load ."

# ===========================================================================================================================================

echo -e "\e[44mRemoving current [$IMAGE_TAG] image ...\e[49m"
eval $remove_docker_image

echo -e "\e[44mBuilding container [$IMAGE_TAG] ...\e[49m"
eval $build_docker

echo -e "\e[44mSerializing image to $IMAGE_TARBALL_FILE ...\e[49m"
eval $save_docker_image